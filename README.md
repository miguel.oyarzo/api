### Requeriments 
- make 3.81+
- kubectl v1.18.2+
- ideally EKS or GKE cluster with Create Load Balancer permissions
- git
- proper credentials to your kubernetes service ($KUBECONFIG)

##### Download the node application from Gitlab
`git clone https://gitlab.com/miguel.oyarzo/api.git`

## Deployment

##### create a namespace, if not yet created (default: technical-test)
`make k8s/init`

##### Check plan of manifests to apply
`make k8s/plan`

##### Install k8s objects
`make k8s/apply`

##### Check resources deployed
`make k8s/show`

##### Check the URL from the domain created by your load balancer
`curl <domain>:8080/version`

##### Delete objects inside of k8s/ directory (namespace won't be deleted).
`make k8s/delete`