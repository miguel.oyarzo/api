FROM node:12

# Create app directory
WORKDIR /usr/src/app
COPY package*.json ./

ARG COMMIT_SHORT_SHA
ENV COMMIT_SHORT_SHA ${COMMIT_SHORT_SHA}
RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .

ENV PORT 8080
EXPOSE ${PORT}
CMD [ "node", "server.js" ]