const COMMIT_SHORT_SHA = process.env.COMMIT_SHORT_SHA || "COMMIT_SHORT_SHA not set";
const { version } = require('./package.json') || "not.defin.ed";
const express = require("express");
const app = express();


app.use(express.json());

app.get("/version", (req, res) => {
  var obj = {};
  obj.version = version;
  obj.commit = COMMIT_SHORT_SHA;
  obj.description = "pre-interview technical test";

  res.json(obj);
});

// default
app.use(function(req, res){
  res.sendStatus(404);
});

const PORT = process.env.PORT || 8080;

app.listen(PORT, () => console.log(`App listening on port ${PORT}`));

module.exports = app;
