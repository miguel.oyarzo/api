KUBECTL=kubectl
NAMESPACE=technical-test
K8S_DIR=k8s

.PHONY: k8s/init
k8s/init:
	@$(eval ns=$(shell kubectl get ns $(NAMESPACE) --no-headers --output=go-template={{.metadata.name}} 2>/dev/null))
	@[[ -z "$(ns)" ]] && $(KUBECTL) create ns $(NAMESPACE) || exit 0


.PHONY: k8s/plan
k8s/plan:
	@$(KUBECTL) -n $(NAMESPACE) diff -f $(K8S_DIR) || exit 0

.PHONY: k8s/apply
k8s/apply:
	@$(KUBECTL) -n $(NAMESPACE) apply -f $(K8S_DIR)

.PHONY: k8s/show
k8s/show:
	@$(KUBECTL) -n $(NAMESPACE) get deploy,service

.PHONY: k8s/diff
k8s/diff:
	@$(KUBECTL) -n $(NAMESPACE) diff -f $(K8S_DIR)

.PHONY: k8s/delete
k8s/delete:
	@$(KUBECTL) -n $(NAMESPACE) delete -f $(K8S_DIR)