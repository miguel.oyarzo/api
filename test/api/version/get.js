// mocha chai chai-http

const app = require("../../../server.js");
const chai = require("chai");
const chaiHttp = require("chai-http");

const { expect } = chai;
chai.use(chaiHttp);

describe("get requests", () => {
  it("expect to contain property [version, commit, description]", done => {
    chai
      .request(app)
      .get("/version")
      .end((err, res) => {
        const body = res.body
        expect(res).to.have.status(200);
        expect(body).to.contain.property('version')
        expect(body).to.contain.property('commit')
        expect(body).to.contain.property('description')
        // TODO
        done();
      });
  });
});